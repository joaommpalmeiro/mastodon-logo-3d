# mastodon-logo-3d

> https://mastodon-logo-3d.pages.dev/

Mastodon 3D logo adapted from the [svelte-logo-3d](https://github.com/ghostdevv/svelte-logo-3d) project developed by [GHOST](https://github.com/ghostdevv).

## References

- https://github.com/vitejs/vite/tree/main/packages/create-vite/template-svelte and https://github.com/vitejs/vite/tree/main/packages/create-vite/template-svelte-ts
- https://vitejs.dev/guide/
- https://joinmastodon.org/branding

## Development

- `npm install`
- `npm run dev`

## Notes

- `npm install -D vite svelte @sveltejs/vite-plugin-svelte`
- https://github.com/sveltejs/prettier-plugin-svelte
- https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode
- `npm install -D three @threlte/core`
  - https://threlte.xyz/
  - https://www.npmjs.com/package/threlte
- `npm install -D svgo`
- Deployment:
  - https://developers.cloudflare.com/pages/framework-guides/deploy-a-vite3-project/
  - `npm run build` + `dist` folder
  - Environment variables:
    - `NODE_VERSION`: `16`
