export const extrudeSettings = {
    curveSegments: 64,
    depth: 2,
    bevelEnabled: true,
    bevelOffset: 0,
    bevelSegments: 64,
    bevelSize: 1,
    bevelThickness: 2,
};

export const colors = {
    outer: '#6364ff',
    inner: '#ffffff',
};
